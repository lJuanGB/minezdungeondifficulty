package com.gmail.lJuanGBMinecraft;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.lJuanGBMinecraft.Listeners.CreatureSpawnListener;
import com.gmail.lJuanGBMinecraft.Listeners.ModifierCreatureSpawnListener;
import com.gmail.lJuanGBMinecraft.Listeners.ModifierEntityDamageListener;
import com.gmail.lJuanGBMinecraft.Listeners.ModifierEntityDeathListener;
import com.gmail.lJuanGBMinecraft.Listeners.WorldUnloadListener;

/**
 * Plugin instance. Removes all settings on disable.
 * 
 * @author lJuanGB
 */
public class MineZDungeonDifficulty extends JavaPlugin{

	@Override
	public void onEnable() {
		
		Bukkit.getServer().getPluginManager().registerEvents(new CreatureSpawnListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ModifierCreatureSpawnListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ModifierEntityDamageListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ModifierEntityDamageListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ModifierEntityDeathListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new WorldUnloadListener(this), this);
		
		
		getCommand("dungeondifficulty").setExecutor(new DungeonDifficultyCommand(this));
	}
	
	@Override
	public void onDisable() {
		
		for(World w : Bukkit.getWorlds()) {
			
			if(w.hasMetadata("difficultySettings")) {

				if(!w.getMetadata("difficultySettings").isEmpty() && 
						w.getMetadata("difficultySettings").get(0).value() instanceof DifficultySettings) {
					
					DifficultySettings setting = (DifficultySettings) w.getMetadata("difficultySettings").get(0).value();
					if(setting != null) {
						setting.getTimer().cancel();
					}
				}
				
				
				w.removeMetadata("difficultySettings", this);
				
			}
			
		}
	}
}
