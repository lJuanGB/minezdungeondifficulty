package com.gmail.lJuanGBMinecraft.Listeners;

import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.lJuanGBMinecraft.DifficultySettings;
import com.gmail.lJuanGBMinecraft.MineZDungeonDifficulty;
import com.gmail.lJuanGBMinecraft.Modifier;

/**
 * Listens to when a creature is damaged to apply the necessary modifiers.
 * 
 * @author lJuanGB
 */
public class ModifierEntityDamageListener implements Listener{
	
	private MineZDungeonDifficulty plugin;
	
	public ModifierEntityDamageListener(MineZDungeonDifficulty plugin) {
		this.plugin = plugin;
	}
	
	
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onEntityTakeDamage(EntityDamageEvent e) {
		
		if(!(e.getEntity() instanceof LivingEntity)) return;
		
		LivingEntity entity = (LivingEntity) e.getEntity();
				
		if(!DifficultySettings.isValid(entity.getType())) return;
		
		DifficultySettings setting = DifficultySettings.getSettings(entity.getWorld(), plugin);
		if(setting == null) return;
		
		
		double finalHealth = entity.getHealth() - e.getFinalDamage();
		
		// For players
		if(entity.getType().equals(EntityType.PLAYER)) {
			
			if(setting.hasModifier(Modifier.PLAYER_BATTLE_RAGE)) {
				
				if(finalHealth < entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue() * 0.3) {
					
					entity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5 *20, 0));
					entity.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 5 *20, 0));
					entity.getWorld().spawnParticle(Particle.VILLAGER_ANGRY, entity.getLocation().clone().add(0, 1.5, 0), 
							 10, 0.5, 0.5, 0.5, 0.05);
				}
				
			}
			
		}
		
		
		// For Zombies
		
		if(entity.getType().equals(EntityType.ZOMBIE)) {
			
			if(setting.hasModifier(Modifier.ZOMBIE_BATTLE_RAGE)) {
				
				if(Modifier.ZOMBIE_BATTLE_RAGE.shouldHappen()) {
					
					if(finalHealth < entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue()* 0.3) {
						
						entity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5 *20, 0));
						entity.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 5 *20, 0));
						entity.getWorld().spawnParticle(Particle.VILLAGER_ANGRY, entity.getLocation().clone().add(0, 1.5, 0), 
								 3, 0.5, 0.5, 0.5, 0.05);
					}
				}
			}
			
			if(setting.hasModifier(Modifier.PINATA)) {
				
				if(Modifier.PINATA.shouldHappen()) {
					
					entity.setHealth(0);
					entity.getWorld().spawnParticle(Particle.REDSTONE, entity.getLocation().clone().add(0, 1, 0), 
							 50, 0.5, 1, 0.5, 0.05);				
				}
			}
			
			if(setting.hasModifier(Modifier.SUPER_PINATA)) {
				
				if(Modifier.SUPER_PINATA.shouldHappen()) {
					
					entity.setHealth(0);
					entity.getWorld().spawnParticle(Particle.REDSTONE, entity.getLocation().clone().add(0, 1, 0), 
							 50, 0.5, 1, 0.5, 0.05);				
				}
			}
			
			if(setting.hasModifier(Modifier.LIVING_DEAD)) {
				
				if(Modifier.LIVING_DEAD.shouldHappen()) {
					
					if(finalHealth <= 0) {
						
						entity.setHealth(entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
						entity.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, entity.getLocation().clone().add(0, 1.5, 0), 
								 10, 0.5, 0.5, 0.5, 0.05);
					}
				}
			}
			
		}
		
	}

}
