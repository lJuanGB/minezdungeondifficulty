package com.gmail.lJuanGBMinecraft.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import com.gmail.lJuanGBMinecraft.DifficultySettings;
import com.gmail.lJuanGBMinecraft.MineZDungeonDifficulty;
import com.gmail.lJuanGBMinecraft.Modifier;

/**
 * Listens to when a creature dies to apply the necessary modifiers.
 * 
 * @author lJuanGB
 */
public class ModifierEntityDeathListener implements Listener{
	
	private MineZDungeonDifficulty plugin;
	
	public ModifierEntityDeathListener(MineZDungeonDifficulty plugin) {
		this.plugin = plugin;
	}
	
	
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDeath(EntityDeathEvent e) {
		
		if(!(e.getEntity() instanceof LivingEntity)) return;
		
		LivingEntity entity = (LivingEntity) e.getEntity();
		
		DifficultySettings setting = DifficultySettings.getSettings(entity.getWorld(), plugin);
		if(setting == null) return;
		
	
		
		// For players
		if(entity.getType().equals(EntityType.PLAYER)) {
			
			if(setting.hasModifier(Modifier.HARM_AT_PLAYER_DEATH)) {
				
				if(Modifier.HARM_AT_PLAYER_DEATH.shouldHappen()) {
					
					for(Player p : entity.getWorld().getPlayers()) {
						p.addPotionEffect(new PotionEffect(PotionEffectType.HARM, 1, 0));
						p.sendMessage(ChatColor.DARK_RED + "A teammate has died!");
					}
				}
			}
			
			if(setting.hasModifier(Modifier.HEAL_AT_PLAYER_DEATH)) {
				
				if(Modifier.HEAL_AT_PLAYER_DEATH.shouldHappen()) {
					
					for(Player p : entity.getWorld().getPlayers()) {
						p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 15 * 20, 0));
						p.sendMessage(ChatColor.DARK_RED + "A teammate has died!");
					}
				}
			}
			
			if(setting.hasModifier(Modifier.KILL_AT_PLAYER_DEATH)) {
				
				if(Modifier.KILL_AT_PLAYER_DEATH.shouldHappen()) {
					
					for(Player p : entity.getWorld().getPlayers()) {
						p.setHealth(0);
						p.sendMessage(ChatColor.DARK_RED + "A teammate has died!");
					}
				}
			}
		}
		
		
		//For Pigmans only
		if(entity.getType().equals(EntityType.PIG_ZOMBIE)) {
			
			if(setting.hasModifier(Modifier.KING_PIGMAN)) {
				
				if(entity.hasMetadata("isKingPigman")){
					
					for(int i = 0; i < 4; i++) {
						PigZombie z = (PigZombie) entity.getWorld().spawnEntity(entity.getLocation(), EntityType.PIG_ZOMBIE);
						z.setBaby(true);
					}
				}

			}
			
		}
		
				
		// For Zombies
		
		if(entity.getType().equals(EntityType.ZOMBIE)) {
			
			if(setting.hasModifier(Modifier.HARM_AT_ZOMBIE_DEATH)) {
				
				if(Modifier.HARM_AT_ZOMBIE_DEATH.shouldHappen()) {
					
					ThrownPotion pot = (ThrownPotion) entity.getWorld().spawnEntity(entity.getLocation(), EntityType.SPLASH_POTION);
					ItemStack potItem = new ItemStack(Material.SPLASH_POTION);
					PotionMeta meta = (PotionMeta) potItem.getItemMeta();
					meta.setBasePotionData(new PotionData(PotionType.INSTANT_DAMAGE));
					potItem.setItemMeta(meta);
					pot.setItem(potItem);
				}
			}
			
			if(setting.hasModifier(Modifier.HEAL_AT_ZOMBIE_DEATH)) {
				
				if(Modifier.HEAL_AT_ZOMBIE_DEATH.shouldHappen()) {
					
					ThrownPotion pot = (ThrownPotion) entity.getWorld().spawnEntity(entity.getLocation(), EntityType.SPLASH_POTION);
					ItemStack potItem = new ItemStack(Material.SPLASH_POTION);
					PotionMeta meta = (PotionMeta) potItem.getItemMeta();
					meta.setBasePotionData(new PotionData(PotionType.INSTANT_HEAL));
					potItem.setItemMeta(meta);
					pot.setItem(potItem);
				}
			}
			
		}
		
	}

}
