package com.gmail.lJuanGBMinecraft.Listeners;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.gmail.lJuanGBMinecraft.DifficultySettings;
import com.gmail.lJuanGBMinecraft.MineZDungeonDifficulty;
import com.gmail.lJuanGBMinecraft.Modifier;


/**
 * Listens to when a creature spawn to apply the necessary modifiers.
 * 
 * @author lJuanGB
 */
public class ModifierCreatureSpawnListener implements Listener{
	
	private MineZDungeonDifficulty plugin;
	
	public ModifierCreatureSpawnListener(MineZDungeonDifficulty plugin) {
		this.plugin = plugin;
	}
	
	
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onCreateSpawn(CreatureSpawnEvent e) {
		LivingEntity entity = e.getEntity();
				
		DifficultySettings setting = DifficultySettings.getSettings(entity.getWorld(), plugin);
		if(setting == null) return;
		
	
		//For all kinds of mobs		
		if(DifficultySettings.isValid(entity.getType())) {
			
			if(setting.hasModifier(Modifier.HIGH_DOUBLE_TROUBLE)) {
				
				if(Modifier.HIGH_DOUBLE_TROUBLE.shouldHappen()) {
					entity.getWorld().spawnEntity(entity.getLocation(), entity.getType());
				}
				
			} else if(setting.hasModifier(Modifier.DOUBLE_TROUBLE)) {
				
				if(Modifier.DOUBLE_TROUBLE.shouldHappen()) {
					entity.getWorld().spawnEntity(entity.getLocation(), entity.getType());
				}
				
			}
			
		}
		
		//For Pigmans only
		if(entity.getType().equals(EntityType.PIG_ZOMBIE)) {
			
			if(setting.hasModifier(Modifier.NO_PIGS)) {
				
				if(Modifier.NO_PIGS.shouldHappen()) {
					entity.getWorld().spawnEntity(entity.getLocation(), EntityType.ZOMBIE);
					entity.remove();
				}

			}
			
			if(setting.hasModifier(Modifier.KING_PIGMAN)) {
				
				if(Modifier.KING_PIGMAN.shouldHappen()) {
					entity.getEquipment().setHelmet(Modifier.KING_PIGMAN.getHead());
					entity.setMetadata("isKingPigman", new FixedMetadataValue(plugin, true));
				}

			}
			
		}

		
		//For zombies only
		
		if(entity.getType().equals(EntityType.ZOMBIE)) {
		
			
			if(setting.hasModifier(Modifier.TOO_MANY_PIGS) && !setting.hasModifier(Modifier.NO_PIGS)) {
				
				if(Modifier.TOO_MANY_PIGS.shouldHappen()) {
					entity.getWorld().spawnEntity(entity.getLocation(), EntityType.PIG_ZOMBIE);
					entity.remove();
				}
				
			} else if (setting.hasModifier(Modifier.EVEN_MORE_PIGS) && !setting.hasModifier(Modifier.NO_PIGS)) {
				
				if(Modifier.EVEN_MORE_PIGS.shouldHappen()) {
					entity.getWorld().spawnEntity(entity.getLocation(), EntityType.PIG_ZOMBIE);
					entity.remove();
				}
				
			} else if (setting.hasModifier(Modifier.MORE_PIGS) && !setting.hasModifier(Modifier.NO_PIGS)) {
				
				if(Modifier.MORE_PIGS.shouldHappen()) {
					entity.getWorld().spawnEntity(entity.getLocation(), EntityType.PIG_ZOMBIE);
					entity.remove();
				}
				
			}
			
			
			if(setting.hasModifier(Modifier.DEFINITELY_NOT_ZOMBIES)) {
				
				if(Modifier.DEFINITELY_NOT_ZOMBIES.shouldHappen()) {
					if(entity.getEquipment().getHelmet().getType().equals(Material.AIR)) {
						entity.getEquipment().setHelmet(Modifier.DEFINITELY_NOT_ZOMBIES.getHead());
					}
				}
			}
			
			if(setting.hasModifier(Modifier.CHRISTMAS)) {
				
				if(Modifier.CHRISTMAS.shouldHappen()) {
					if(entity.getEquipment().getHelmet().getType().equals(Material.AIR)) {
						entity.getEquipment().setHelmet(Modifier.CHRISTMAS.getHead());
					}
				}
			}
			
			if(setting.hasModifier(Modifier.SUMMER)) {
				
				if(Modifier.SUMMER.shouldHappen()) {
					if(entity.getEquipment().getHelmet().getType().equals(Material.AIR)) {
						entity.getEquipment().setHelmet(Modifier.SUMMER.getHead());
					}
				}
			}
			
			
		}
	}
}

