package com.gmail.lJuanGBMinecraft.Listeners;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import com.gmail.lJuanGBMinecraft.DifficultySettings;
import com.gmail.lJuanGBMinecraft.MineZDungeonDifficulty;

/**
 * Listenes to whenever a creature spawns and applies the corresponding attributes modifications and effects.
 * 
 * @author lJuanGB
 */
public class CreatureSpawnListener implements Listener{
	
	private MineZDungeonDifficulty plugin;
	
	public CreatureSpawnListener(MineZDungeonDifficulty plugin) {
		this.plugin = plugin;
	}
	
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onCreateSpawn(CreatureSpawnEvent e) {
		LivingEntity entity = e.getEntity();
		
		if(!DifficultySettings.isValid(entity.getType())) return;
		
		DifficultySettings setting = DifficultySettings.getSettings(entity.getWorld(), plugin);
		if(setting == null) return;
		
		
		if(entity.getAttribute(Attribute.GENERIC_ARMOR) != null) {
			double armor = entity.getAttribute(Attribute.GENERIC_ARMOR).getBaseValue();
			double newArmor = setting.getAttributeValue(entity.getType(), Attribute.GENERIC_ARMOR, armor);
			newArmor = Math.min(30, Math.max(0, newArmor));
			entity.getAttribute(Attribute.GENERIC_ARMOR).setBaseValue(newArmor);
		}
		
		if(entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE) != null) {
			double damage = entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).getBaseValue();
			double newDamage = setting.getAttributeValue(entity.getType(), Attribute.GENERIC_ATTACK_DAMAGE, damage);
			newDamage = Math.min(2048, Math.max(0, newDamage));
			entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(newDamage);
		}
		
		if(entity.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE) != null) {
			double kb = entity.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE).getBaseValue();
			double newKB = setting.getAttributeValue(entity.getType(), Attribute.GENERIC_KNOCKBACK_RESISTANCE, kb);
			newKB = Math.min(1, Math.max(0, newKB));
			entity.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE).setBaseValue(newKB);
		}
		
		if(entity.getAttribute(Attribute.GENERIC_MAX_HEALTH) != null) {
			double health = entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
			double newHealth = setting.getAttributeValue(entity.getType(), Attribute.GENERIC_MAX_HEALTH, health);
			newHealth = Math.min(1024, Math.max(0.1, newHealth));
			entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(newHealth);
			entity.setHealth(newHealth);
		}
		
		
		
		entity.addPotionEffects( setting.getEffects(entity.getType()) );
	}
}
