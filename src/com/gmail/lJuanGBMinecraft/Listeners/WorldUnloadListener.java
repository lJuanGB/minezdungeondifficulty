package com.gmail.lJuanGBMinecraft.Listeners;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldUnloadEvent;

import com.gmail.lJuanGBMinecraft.DifficultySettings;
import com.gmail.lJuanGBMinecraft.MineZDungeonDifficulty;

/**
 * Safety class. Removes the difficulty settings once the world unloads. Should prevent the difficulty settings
 * from previous runs from loading when the world is created again. In any way, it's better practice to use the command
 * '/dd reset' whenever the dungeon starts to reset all previously set values.
 * 
 * @author lJuanGB
 */
public class WorldUnloadListener implements Listener{

private MineZDungeonDifficulty plugin;
	
	public WorldUnloadListener(MineZDungeonDifficulty plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onWorldUnload(WorldUnloadEvent e) {
		
		World w = e.getWorld();
		
		DifficultySettings.removeSettings(w, plugin);

	}
}
