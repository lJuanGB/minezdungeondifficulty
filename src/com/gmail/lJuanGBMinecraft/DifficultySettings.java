package com.gmail.lJuanGBMinecraft;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Holds all the settings for a current dungeon. This is stored in the metadata of the world.
 * 
 * @author lJuanGB
 */
public class DifficultySettings {

	private HashMap<EntityType, HashMap<Attribute, Double> > addAttributes;
	private HashMap<EntityType, HashMap<Attribute, Double> > multiplyAttributes;
	private HashMap<EntityType, HashMap<Attribute, Double> > setAttributes;

	
	private HashMap<EntityType, List<PotionEffect> > effects;
	
	private TimerRunnable timer;
	
	private List<Modifier> modifiers;
	
	
	@SuppressWarnings("unchecked")
	public DifficultySettings(World world, MineZDungeonDifficulty plugin) {
		
		addAttributes = new HashMap<EntityType, HashMap<Attribute, Double> >();
		multiplyAttributes = new HashMap<EntityType, HashMap<Attribute, Double> >();
		setAttributes = new HashMap<EntityType, HashMap<Attribute, Double> >();
		
		HashMap<Attribute, Double> emptyAttributeMap = new HashMap<Attribute, Double>();
		emptyAttributeMap.put(Attribute.GENERIC_ARMOR, 0.0);
		emptyAttributeMap.put(Attribute.GENERIC_KNOCKBACK_RESISTANCE, 0.0);
		emptyAttributeMap.put(Attribute.GENERIC_MAX_HEALTH, 0.0);
		emptyAttributeMap.put(Attribute.GENERIC_ATTACK_DAMAGE, 0.0);
		
		for(EntityType entType : validEntities) {
			addAttributes.put(entType, (HashMap<Attribute, Double>) emptyAttributeMap.clone());
			multiplyAttributes.put(entType, (HashMap<Attribute, Double>) emptyAttributeMap.clone());
			setAttributes.put(entType, (HashMap<Attribute, Double>) emptyAttributeMap.clone());
		}
		
		
		effects = new HashMap<EntityType, List<PotionEffect> >();
		
		for(EntityType entType : validEntities) {
			effects.put(entType, new ArrayList<PotionEffect>());
		}
		
		
		this.timer = new TimerRunnable(world, PunishmentType.NOTHING, false, plugin);
		
		this.modifiers = new ArrayList<Modifier>();
		
		world.setMetadata("difficultySettings", new FixedMetadataValue(plugin, this));
	}
	
	
	
	
	public double getAttributeValue(EntityType type, Attribute attribute, double baseValue) {
		
		double add = this.addAttributes.get(type).get(attribute);
		double set = this.setAttributes.get(type).get(attribute);
		double multiply = this.multiplyAttributes.get(type).get(attribute);

		if(set != 0.0) {
			return set;
		}
		
		if(multiply != 0.0) {
			baseValue = baseValue * multiply;
		}
		if(add != 0.0) {
			baseValue = baseValue + add;
		}
		
		return baseValue;
	}
	
	public double getAttributeValue(EntityType type, Attribute attribute, String operation) {
		
		double add = this.addAttributes.get(type).get(attribute);
		double set = this.setAttributes.get(type).get(attribute);
		double multiply = this.multiplyAttributes.get(type).get(attribute);
		
		switch(operation) {
		case "add": return add;
		case "set": return set;
		case "multiply": return multiply;
		default: return 0;
		}
	}
	
	public void setAttributeValue(EntityType type, Attribute attribute, double value) {
				
		HashMap<Attribute, Double> map = this.setAttributes.get(type);
		map.put(attribute, value);

		this.setAttributes.put(type, map);
	}
	
	public void addAttributeValue(EntityType type, Attribute attribute, double value) {
		HashMap<Attribute, Double> map = this.addAttributes.get(type);
		map.put(attribute, value);
		
		this.addAttributes.put(type, map);
		System.out.println(map);
	}
	
	public void multiplyAttributeValue(EntityType type, Attribute attribute, double value) {
		HashMap<Attribute, Double> map = this.multiplyAttributes.get(type);
		map.put(attribute, value);
		
		this.multiplyAttributes.put(type, map);
	}
	
	
	public void setAttributeValue(Attribute attribute, double value) {
		
		for(EntityType entType : validEntities) {
			this.setAttributeValue(entType, attribute, value);
		}
		
	}
	
	public void addAttributeValue(Attribute attribute, double value) {
		
		for(EntityType entType : validEntities) {
			this.addAttributeValue(entType, attribute, value);
		}
		
	}
	
	public void multiplyAttributeValue(Attribute attribute, double value) {
		
		for(EntityType entType : validEntities) {
			this.multiplyAttributeValue(entType, attribute, value);
		}
		
	}
	
	
	
	public List<PotionEffect> getEffects(EntityType type) {
		return this.effects.get(type);
	}
	
	public void addEffect(EntityType type, PotionEffect effect) {
		this.effects.get(type).add(effect);
	}
	
	public void addEffect(EntityType entType, PotionEffectType potType, int duration, int amplifier) {
		this.addEffect(entType, new PotionEffect(potType, Integer.MAX_VALUE, 0));
	}
	
	public void addEffect(EntityType entType, PotionEffectType potType, int amplifier) {
		this.addEffect(entType, potType, Integer.MAX_VALUE, amplifier);
	}
	
	public void addEffect(EntityType entType, PotionEffectType potType) {
		this.addEffect(entType, potType, 0);
	}
	
	
	public void addEffect(PotionEffect effect) {
		
		for(EntityType entType : validEntities) {
			this.addEffect(entType, effect);
		}
		
	}
	
	public void addEffect(PotionEffectType potType, int duration, int amplifier) {
		this.addEffect(new PotionEffect(potType, Integer.MAX_VALUE, 0));
	}
	
	public void addEffect(PotionEffectType potType, int amplifier) {
		this.addEffect(potType, Integer.MAX_VALUE, amplifier);
	}
	
	public void addEffect(PotionEffectType potType) {
		this.addEffect(potType, 0);
	}
	
	public void removeEffect(EntityType entType, PotionEffectType potType) {
		
		for(PotionEffect effect : this.getEffects(entType).toArray(new PotionEffect[0])) {
			
			if(effect.getType().equals(potType)) {
				
				this.getEffects(entType).remove(effect);
			}
			
		}
	}
	
	public void removeEffect(PotionEffectType potType) {
		
		for(EntityType entType : validEntities) {

			this.removeEffect(entType, potType);
		}
	}
	
	
	public TimerRunnable getTimer() {
		return this.timer;
	}
	
	
	
	public List<Modifier> getModifiers() {
		return this.modifiers;
	}
	
	public boolean hasModifier(Modifier modifier) {
		return this.modifiers.contains(modifier);
	}
	
	public void addModifier(Modifier modifier) {
		if(this.hasModifier(modifier)) return;
		
		this.modifiers.add(modifier);
	}
	
	public void removeModifier(Modifier modifier) {
		this.modifiers.remove(modifier);
	}


	
	public static DifficultySettings getSettings(World world, MineZDungeonDifficulty plugin) {
		
		if(!world.hasMetadata("difficultySettings")) return null;
		if(world.getMetadata("difficultySettings").isEmpty()) return null;
		if(!(world.getMetadata("difficultySettings").get(0).value() instanceof DifficultySettings)) return null;
		
		return (DifficultySettings) world.getMetadata("difficultySettings").get(0).value();
		
	}
	
	public static void removeSettings(World world, MineZDungeonDifficulty plugin) {
		
		DifficultySettings setting = getSettings(world, plugin);
		if(setting != null) setting.getTimer().cancel();
			
		world.removeMetadata("difficultySettings", plugin);

		
	}
	
	
	
	public static EntityType[] validEntities = new EntityType[] {EntityType.BLAZE, EntityType.CAVE_SPIDER, EntityType.CREEPER,
			EntityType.ENDERMAN, EntityType.ENDERMITE, EntityType.GHAST, EntityType.GIANT, EntityType.GUARDIAN,
			EntityType.MAGMA_CUBE, EntityType.PIG_ZOMBIE, EntityType.SHULKER, EntityType.SILVERFISH, EntityType.SKELETON,
			EntityType.SLIME, EntityType.SPIDER, EntityType.WITCH, EntityType.ZOMBIE};

	
	public static boolean isValid(EntityType type) {
		for(EntityType valid : validEntities) {
			if(type.equals(valid)) return true;
		}
		
		return false;
	}
}
