package com.gmail.lJuanGBMinecraft;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * The command /dungeondifficulty. There are 7 subcategories: help, info, reset, attribute, effect, time and modifier.
 * 
 * @author lJuanGB
 */
public class DungeonDifficultyCommand implements CommandExecutor{
	
	private MineZDungeonDifficulty plugin;
	
	public DungeonDifficultyCommand(MineZDungeonDifficulty plugin) {
		this.plugin = plugin;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {

		if(!cmd.getName().equals("dungeondifficulty")) return false;
		
		Location loc = null;
		
		if (sender instanceof Player) {
			if(!sender.hasPermission("minez.dungeondifficulty.command")) {
				return true;
			}
				
			loc = ((Player) sender).getLocation();
		
		} else if (sender instanceof BlockCommandSender) {
			loc = ((BlockCommandSender) sender).getBlock().getLocation();
			
		} else {
			return false;
		}
		
		
		DifficultySettings setting = DifficultySettings.getSettings(loc.getWorld(), plugin);
		if(setting == null) setting = new DifficultySettings(loc.getWorld(), plugin);
		
		
		
		if(args.length == 0) return false;
		
		switch ( args[0].toLowerCase()) {
		case "help":
			
			if (!(sender instanceof Player)) return false;
			Player player = (Player) sender;
			
			player.sendMessage("");

			if(args.length == 1) {
				player.sendMessage(ChatColor.GOLD + plugin.getDescription().getDescription());
				player.sendMessage(ChatColor.GOLD + "Do '/dd help <module>' with any of the following modules to get help on how to use them:");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "reset: " + ChatColor.DARK_AQUA +
						"Sets all fields to the standard value. Recovers the default settings.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "attribute: " + ChatColor.DARK_AQUA +
										"Easily alter the armor, health, damage and knockback resistance of all spawned mobs.");
				
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "effect: " + ChatColor.DARK_AQUA +
										"Easily add potion effects to all spawned mobs.");
				
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "time: " + ChatColor.DARK_AQUA +
										"Easy customizable timing system.");
				
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "modifier: " + ChatColor.DARK_AQUA +
										"Turn different modifiers on and off.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "info: " + ChatColor.DARK_AQUA +
										"Displays the current world settings.");
				
				return true;
			}
			
			switch(args[1].toLowerCase()) {
			case "reset":
				player.sendMessage(ChatColor.RED + "USAGE: /dd reset" );
				player.sendMessage(ChatColor.GOLD + "It is best practise to use it at the begginig of a dungeon to " + 
						"remove any possible remaining settings from previous runs. Can be used at any point to reset the settings.");
				
				return true;
				
			case "attribute":
				if(args.length == 2) {
					
					player.sendMessage(ChatColor.RED + "USAGE: /dd attribute <mobType> <attribute> <add, set, multiply> <value>" );
					player.sendMessage(ChatColor.GOLD + "Whenever a mob of the type defined at <mobType> spawns, the attribute defined " + 
							"at <attribute> is set to the value by multiplying its normal value by the value defined using <multiply>, then "+
							"adding the value defined at <add> or directly set to a value if <set> was used. Use '/dd help attribute example' for an example.");
					
					player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "mobType: " + ChatColor.DARK_AQUA +
							"Zombie, skeleton, pigman... Using '*' sets it for all mobs. ");
					player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "attribute: " + ChatColor.DARK_AQUA +
							"Health, armor, damage or knockbackResistance");
					player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "add, set, multiply: " + ChatColor.DARK_AQUA +
							"The operator that must be used. Has to one of those 3.");
					player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "value: " + ChatColor.DARK_AQUA +
							"The value to be added, multiply or set. Can be a decimal. If you want to remove a previously set value, set this to 0.");
					
				} else if (args[2].equals("example")) {
					
					player.sendMessage(ChatColor.GOLD + "Imagine the following commands are activated: ");
					player.sendMessage("/dd attribute zombie health multiply 1.2");
					player.sendMessage("/dd attribute zombie health add 3");
					player.sendMessage("/dd attribute skeleton damage set 7.5");
					player.sendMessage("/dd attribute skeleton damage add 999");

					player.sendMessage(ChatColor.DARK_AQUA + "This means all zombies that spawn, whose normal health is let's say is normally " +
							"10, will have its health multiplied by 1.2 (so 12 health) and then added a flat 3 (so 15 final health).");
					player.sendMessage(ChatColor.DARK_AQUA + "It also means that for all skeletons the damage will be 7.5, independent on " + 
							"its previous values or add and multiply commands.");

				}
				
				return true;
			
			case "effect":
				player.sendMessage(ChatColor.RED + "USAGE: /dd effect <mobType> <effectType> [amplifier] [duration]" );
				player.sendMessage(ChatColor.GOLD + "Whenever a mob of the type defined at <mobType> spawns, they are " +
				"applied the effect defined by this command. ");
				
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "mobType: " + ChatColor.DARK_AQUA +
						"Zombie, skeleton, pigman... Using '*' sets it for all mobs. ");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "effectType: " + ChatColor.DARK_AQUA +
						"The effect type as defined in the bukkit API or the minecraft wiki");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "amplifier: " + ChatColor.DARK_AQUA +
						"The amplifier this effect will have. If not defined it defaults to 0.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "duration: " + ChatColor.DARK_AQUA +
						"The duration in ticks for the effect. If not defined it defaults to infinite. Set to 0 to remove previously " + 
						"set effects with the same type for this mob type.");
				
				return true;
				
			case "time":
				player.sendMessage(ChatColor.RED + "USAGE: " );

				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd time start: " + ChatColor.DARK_AQUA +
						"Starts (or resumes) the clock.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd time stop: " + ChatColor.DARK_AQUA +
						"Stops the clock.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd time test <min> <max>: " + ChatColor.DARK_AQUA +
						"Test if the remaining time until the clock runs out is between <min> and <max>");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd time set <value>: " + ChatColor.DARK_AQUA +
						"Sets the maximum amount of time (in seconds) until the clock runs out.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd time add <value>: " + ChatColor.DARK_AQUA +
						"Adds a value to the time (in seconds) until the clock runs out. Can be negative to shorten the remaining time.");
				
				String punishmentString = ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd time punishment <punishmentType>: " + ChatColor.DARK_AQUA +
						"From now it can be: ";
				for(PunishmentType type : PunishmentType.values()) {
					punishmentString = punishmentString + type.name() + ", ";
				}
				player.sendMessage(punishmentString);
				
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd time display <true/false>: " + ChatColor.DARK_AQUA +
						"If the time should be displayed as a boss bar for the players in the dungeon.");
				
				return true;
			
			case "modifier":
				player.sendMessage(ChatColor.RED + "USAGE: /dd modifier <modifier> <true/false>" );
				player.sendMessage(ChatColor.GOLD + "Applies custom effects to the dungeon. Right now the valid modifiers are:");
				
				for(Modifier mod : Modifier.values()) {
					
					player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + mod.name().toLowerCase() + ": " + 
							ChatColor.DARK_AQUA + mod.getDescription() + " (" + mod.getChance()*100 + "% chance)");
					
				}
				
				return true;
			
			case "info":
				player.sendMessage(ChatColor.RED + "USAGE: " );

				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd info attribute: " + ChatColor.DARK_AQUA +
						"Displays all info for the attribute changes of all entities. Missing values are assumed to be 0.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd info effect: " + ChatColor.DARK_AQUA +
						"Displays all effects that are to be set to each and every entity.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd info time: " + ChatColor.DARK_AQUA +
						"Displays whether the clock is running, the max time, the consumed time, the punishment type and wether it's should display.");
				player.sendMessage(ChatColor.DARK_AQUA + "  - " + ChatColor.WHITE + "/dd info modifier: " + ChatColor.DARK_AQUA +
						"Displays all active modifiers.");
				
				return true;
			}

			break;
		case "info":
			
			if (!(sender instanceof Player)) return false;
			player = (Player) sender;
			
			if(args.length == 1) {
				player.sendMessage(ChatColor.RED + "USAGE: /dd info <attribute, effect, time, modifer>" );
				return false;
			} 
			
			switch(args[1].toLowerCase()) {
			case "attribute":
				
				boolean empty = true;
				
				for(EntityType type : DifficultySettings.validEntities) {
					
					String finalString = ChatColor.BOLD + type.name() + ": \n" + ChatColor.RESET;
					
					String armor = getAttributeString(setting, type, Attribute.GENERIC_ARMOR);
					String damage = getAttributeString(setting, type, Attribute.GENERIC_ATTACK_DAMAGE);
					String kb = getAttributeString(setting, type, Attribute.GENERIC_KNOCKBACK_RESISTANCE);
					String health = getAttributeString(setting, type, Attribute.GENERIC_MAX_HEALTH);
					
					if(armor == null && damage == null && kb == null && health == null) continue;
					
					if(armor != null) finalString = finalString + armor + "\n";
					if(damage != null) finalString = finalString + damage + "\n";
					if(kb != null) finalString = finalString + kb + "\n";
					if(health != null) finalString = finalString + health + "\n";
					
					player.sendMessage(finalString);
					empty = false;
				}
				
				if(empty) player.sendMessage("There are no attributes set yet.");
				return true;
			
			case "effect":
				
				empty = true;
				for(EntityType type : DifficultySettings.validEntities) {
					
					String finalString = ChatColor.BOLD + type.name() + ": \n" + ChatColor.RESET;
					
					if(setting.getEffects(type).isEmpty()) continue;
					
					for(PotionEffect effect : setting.getEffects(type)) {
						finalString = finalString + effectToString(effect) + "\n";
					}
					
					player.sendMessage(finalString);

				}
				
				if(empty) player.sendMessage("There are no effects set yet.");
				return true;
				
			case "time":
				String finalString = ChatColor.BOLD + "Clock Information: \n" + ChatColor.RESET;
				
				if(setting.getTimer().isRunning()) {
					finalString = finalString + "Running: " + ChatColor.GREEN + "TRUE \n" + ChatColor.RESET;
				} else {
					finalString = finalString + "Running: " + ChatColor.RED + "FALSE \n" + ChatColor.RESET;
				}

				finalString = finalString + "Max Time: " + setting.getTimer().getMaxTime() + "\n";
				finalString = finalString + "Consumed Time: " + setting.getTimer().getConsumedTime() + "\n";
				finalString = finalString + "Punishment: " + setting.getTimer().getPunishment().name() + "\n";
				
				if(setting.getTimer().isDisplaying()) {
					finalString = finalString + "Display: " + ChatColor.GREEN + "TRUE \n" + ChatColor.RESET;
				} else {
					finalString = finalString + "Display: " + ChatColor.RED + "FALSE \n" + ChatColor.RESET;
				}

				player.sendMessage(finalString);
				
				return true;
				
			case "modifier":
				
				if(setting.getModifiers().isEmpty()) {
					player.sendMessage("No active modifiers.");
					
				} else {
					
					player.sendMessage("Active modifiers: ");
					for(Modifier modifier : setting.getModifiers()) {
						player.sendMessage(modifier.name());
					}
				}
				
				return true;
				
			default: return false;
			}
		
			
		case "reset":
			
			setting.getTimer().cancel();
			loc.getWorld().removeMetadata("difficultySettings", plugin);
			
			return true;
			
		case "attribute":
			
			if(args.length < 5) return false;
			
			EntityType entType = EntityType.UNKNOWN;
			if(!args[1].equals("*") && !args[1].toLowerCase().equals("pigman")) {
				
				try {
					entType = EntityType.valueOf(args[1].toUpperCase());
				} catch (IllegalArgumentException ex) {
					sender.sendMessage(ChatColor.RED + "That entity does not exist");
					return false;
				}
				
				if(!DifficultySettings.isValid(entType)) {
					sender.sendMessage(ChatColor.RED + "Only mobs can be assigned attributes");
					return true;
				}
			}
			if(args[1].toLowerCase().equals("pigman")) {
				entType = EntityType.PIG_ZOMBIE;
			}
			
			Attribute attribute = getAttribute(args[2]);
			if(attribute == null) return false;
			
			String operation = args[3].toLowerCase();
			if(!(operation.equals("add") || operation.equals("set") || operation.equals("multiply"))) return false;
			
			double value = 0;
			try {
				value = Double.parseDouble(args[4]);
			} catch (NumberFormatException  ex) {
				return false;
			}
			
			
			if(operation.equals("add")) {
				if(entType.equals(EntityType.UNKNOWN)) {
					setting.addAttributeValue(attribute, value);
				} else {
					setting.addAttributeValue(entType, attribute, value);

				}
			}
			
			if(operation.equals("set")) {
				if(entType.equals(EntityType.UNKNOWN)) {
					setting.setAttributeValue(attribute, value);
				} else {
					setting.setAttributeValue(entType, attribute, value);

				}
			}
			
			if(operation.equals("multiply")) {
				if(entType.equals(EntityType.UNKNOWN)) {
					setting.multiplyAttributeValue(attribute, value);
				} else {
					setting.multiplyAttributeValue(entType, attribute, value);

				}
			}
			
			if(value == 0) {
				sender.sendMessage(ChatColor.DARK_AQUA + "Removed the " + args[3] + " for the attribute '" + args[2] + "' for " + args[1]);
			} else {
				sender.sendMessage(ChatColor.DARK_AQUA + WordUtils.capitalize(args[3]) + " attribute '" + args[2] + "' to/by " + value + " for " + entType.name());
			}
			return true;
			
		case "effect":
			
			if(args.length < 3) return false;
			
			entType = EntityType.UNKNOWN;
			if(!args[1].equals("*") && !args[1].toLowerCase().equals("pigman")) {
				
				try {
					entType = EntityType.valueOf(args[1].toUpperCase());
				} catch (IllegalArgumentException ex) {
					sender.sendMessage(ChatColor.RED + "That entity does not exist");
					return false;
				}
				
				if(!DifficultySettings.isValid(entType)) {
					sender.sendMessage(ChatColor.RED + "Only mobs can be assigned attributes");
					return true;
				}
			}
			if(args[1].toLowerCase().equals("pigman")) {
				entType = EntityType.PIG_ZOMBIE;
			}
			
			PotionEffectType effectType = getEffectType(args[2].toUpperCase());
			if(effectType == null) return false;
			
			int amplifier = 0;
			if(args.length >= 4) {
				try {
					amplifier = Integer.parseInt(args[3]);
				} catch (NumberFormatException  ex) {
					//ignore, let amplifier be 0
				}
			}
			
			int duration = Integer.MAX_VALUE;
			if(args.length >= 5) {
				try {
					duration = Integer.parseInt(args[4]);
				} catch (NumberFormatException  ex) {
					//ignore, let duration be max
				}
			}
			
			
			if(duration == 0) {
				if(entType.equals(EntityType.UNKNOWN)) {
					setting.removeEffect(effectType);
				} else {
					setting.removeEffect(entType, effectType);
				}
				
				sender.sendMessage(ChatColor.DARK_AQUA + "Removed effect " + args[2] + " for " + args[1]);
				return true;
			}
			
			PotionEffect effect = new PotionEffect(effectType, duration, amplifier);
			if(entType.equals(EntityType.UNKNOWN)) {
				setting.addEffect(effect);
			} else {
				setting.addEffect(entType, effect);
			}
			
			sender.sendMessage(ChatColor.DARK_AQUA + "Added effect " + args[2] +  " for " + args[1]);
			return true;
			
		case "time":
			
			if(args.length < 2) return false;
			
			switch (args[1].toLowerCase()) {
			case "start":
				
				if(setting.getTimer().isRunning()) {
					
					return true;
				} else {
					
					setting.getTimer().start();
					sender.sendMessage(ChatColor.DARK_AQUA + "Started the clock");
					return true;
				}
				
			case "stop":
				
				if(!setting.getTimer().isRunning()) {
					
					return true;
				} else {
					
					setting.getTimer().stop();
					sender.sendMessage(ChatColor.DARK_AQUA + "Stopped the clock");
					return true;
				}
				
			case "test":
				
				if(args.length < 3) return false;
				
				int min = 0;
				try {
					min = Integer.parseInt(args[2]);
				} catch (NumberFormatException  ex) {
					return false;
				}
				
				int max = Integer.MAX_VALUE;
				if(args.length > 3) {
					try {
						max = Integer.parseInt(args[3]);
					} catch (NumberFormatException  ex) {
						return false;
					}
				}
				
				int timeLeft = setting.getTimer().getTimeLeft();
				sender.sendMessage(ChatColor.DARK_AQUA + "The current remaining seconds are: " + timeLeft);
				
				if(min <= timeLeft && timeLeft <= max) {
					activateBlock(sender);
				}
				
				return true;
				
			case "set":
				
				if(args.length < 3) return false;
				
				int timeValue = 0;
				try {
					timeValue = Integer.parseInt(args[2]);
				} catch (NumberFormatException  ex) {
					return false;
				}
				
				sender.sendMessage(ChatColor.DARK_AQUA + "Set max time to " + timeValue);
				setting.getTimer().setMaxTime(timeValue);
				return true;
				
			case "add":
				
				if(args.length < 3) return false;
				
				timeValue = 0;
				try {
					timeValue = Integer.parseInt(args[2]);
				} catch (NumberFormatException  ex) {
					return false;
				}
				
				sender.sendMessage(ChatColor.DARK_AQUA + "Added time until clock finishes: " + timeValue);
				setting.getTimer().addConsumedTime(-timeValue);
				return true;
				
			case "punishment":
				
				if(args.length < 3) return false;
				
				PunishmentType punishment = PunishmentType.NOTHING;
				try {
					punishment = PunishmentType.valueOf(args[2].toUpperCase());
				} catch (IllegalArgumentException  ex) {
					return false;
				}
				
				sender.sendMessage(ChatColor.DARK_AQUA + "Set punishment " + punishment.name());
				setting.getTimer().setPunishment(punishment);
				return true;
				
			case "display":
				
				boolean display = true;
				if(args.length > 2 && args[2].equalsIgnoreCase("false")) display = false;
					
				if(display) {
					
					sender.sendMessage(ChatColor.DARK_AQUA + "Now displaying clock");
					setting.getTimer().setDisplay(true);
					return true;
					
				} else {
					
					sender.sendMessage(ChatColor.DARK_AQUA + "Now NOT displaying clock");
					setting.getTimer().setDisplay(false);
					return true;
					
				}
				
			default: return false;
			
			}
			
		case "modifier":	

			if(args.length < 2) return false;

			Modifier modifier = null;
			try {
				modifier = Modifier.valueOf(args[1].toUpperCase());
			} catch (IllegalArgumentException  ex) {
				return false;
			}
			
			boolean mod = true;
			if(args.length > 2 && args[2].equalsIgnoreCase("false")) mod = false;
			
			if(mod) {
				
				sender.sendMessage(ChatColor.DARK_AQUA + "Added modifier: " + modifier.name() + ":");
				sender.sendMessage(modifier.getDescription() + " (" + modifier.getChance()*100 + "% chance)");
				setting.addModifier(modifier);
				return true;
				
			} else {
				
				sender.sendMessage(ChatColor.DARK_AQUA + "Removed modifier: " + modifier.name());
				setting.removeModifier(modifier);
				return true;
				
			}
		
		default: return false;
		}
		
		return false;
	}

	
	private PotionEffectType getEffectType(String name) {
		
		switch(name) {
		case "SLOWNESS": return PotionEffectType.SLOW;
		case "HASTE": return PotionEffectType.FAST_DIGGING;
		case "MINING_FATIGUE": return PotionEffectType.SLOW_DIGGING;
		case "STRENGTH": return PotionEffectType.INCREASE_DAMAGE;
		case "INSTANT_HEALTH": return PotionEffectType.HEAL;
		case "INSTANT_DAMAGE": return PotionEffectType.HARM;
		case "JUMP_BOOST": return PotionEffectType.JUMP;
		case "NAUSEA": return PotionEffectType.CONFUSION;
		case "RESISTANCE": return PotionEffectType.DAMAGE_RESISTANCE;
		case "BAD_LUCK": return PotionEffectType.UNLUCK;
		}
		
		return PotionEffectType.getByName(name);
	}


	private void activateBlock(CommandSender sender) {
		if(!(sender instanceof BlockCommandSender)) return;
		
		Block block = ((BlockCommandSender) sender).getBlock();
		BlockFace[] faces = new BlockFace[] {BlockFace.EAST, BlockFace.WEST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.UP, BlockFace.DOWN};
		
		for(BlockFace face : faces) {
			Block relative = block.getRelative(face);
			
			if(relative.getType().equals(Material.GLASS) ) {
				relative.setType(Material.REDSTONE_BLOCK);
				
				Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {

					@Override
					public void run() {
						relative.setType(Material.GLASS);
					}
					
				}, 1L);
			}

		}
		
	}


	private Attribute getAttribute(String name) {
		switch(name.toLowerCase()) {
		case "health":
			return Attribute.GENERIC_MAX_HEALTH;
			
		case "armor":
			return Attribute.GENERIC_ARMOR;
			
		case "knockback":
		case "knockbackresistance":
			return Attribute.GENERIC_KNOCKBACK_RESISTANCE;
			
		case "damage":
			return Attribute.GENERIC_ATTACK_DAMAGE;
			
		default: return null;
		}
	}


	private String effectToString(PotionEffect effect) {
		String result = "";
		String duration;
		
		if(effect.getDuration() == Integer.MAX_VALUE) {
			duration = "infinite";
		} else {
			duration = effect.getDuration() / 20 + " seconds";
		}
		result = result + effect.getType().getName() + " " + effect.getAmplifier() + " (" + duration + ")";
		return result;
	}


	private String getAttributeString(DifficultySettings setting, EntityType type, Attribute attribute) {
		
		double add = setting.getAttributeValue(type, attribute, "add");
		double set = setting.getAttributeValue(type, attribute, "set");
		double multiply = setting.getAttributeValue(type, attribute, "multiply");
		
		String string = "";
		if(set == 0) {
			if(multiply != 0) string = string + "x" + multiply + "  ";
			if(add != 0) string = string + "+" + add;
		} else {
			string = string + "=" + set;
		}
		
		if(!string.equals("")) {
			switch(attribute) {
			case GENERIC_ARMOR: string = "Armor: " + string;  break;
			case GENERIC_ATTACK_DAMAGE: string = "Damage: " + string;  break;
			case GENERIC_KNOCKBACK_RESISTANCE: string = "Knockback Resistance: " + string;  break;
			case GENERIC_MAX_HEALTH: string = "Health: " + string;  break;
			default: return null;
			}
			
			return string;
		}
		
		return null;
	}
}
