package com.gmail.lJuanGBMinecraft;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Runnable that handles the dungeon clock.
 * 
 * @author lJuanGB
 */
public class TimerRunnable extends BukkitRunnable{

	private World world;
	
	private boolean run = false;
	
	private int maxTime = 0;
	private int consumedTime = 0;
	private PunishmentType punishment;
	
	private BossBar bar;
	
	
	public TimerRunnable(World world, PunishmentType punishment, boolean display, MineZDungeonDifficulty plugin) {
		
		 this.world = world;
		 this.punishment = punishment;
		 
		 this.bar = Bukkit.createBossBar("Time Left: ", BarColor.GREEN, BarStyle.SEGMENTED_10);
		 bar.setVisible(false);

		 this.runTaskTimer(plugin, 20, 20);
	}


	@Override
	public void run() {
			
		bar.removeAll();
		if(!world.getPlayers().isEmpty()) {
			 for(Player p : world.getPlayers()) {
				 bar.addPlayer(p);
			 }
		}
		
		if(!run) return;
		
		if(maxTime <= 0) return;
		
		//The out of time events happen every second as long as the time is over
		if(consumedTime >= maxTime) {
			outOfTime();
			return;
		}
		
		consumedTime += 1;
		
		changeBossBar(maxTime - consumedTime);
		
		//A message is sent only once, once the time is over.
		if(consumedTime >= maxTime) {
			
			List<Player> players = world.getPlayers();
			if(!players.isEmpty()) {
				 for(Player p : players) {
					 p.sendMessage(ChatColor.RED + "Run out of time!");
				 }
			}
			
		}
	}
	
	
	public void start() {
		this.run = true;
	}
	
	public void stop() {
		this.run = false;
	}
	
	public int getMaxTime() {
		return this.maxTime;
	}
	
	public int getConsumedTime() {
		return this.consumedTime;
	}
	
	public int getTimeLeft() {
		return (this.maxTime - this.consumedTime);
	}
	
	public void setMaxTime(int value) {
		this.maxTime = value;
	}
	
	public void addConsumedTime(int value) {
		this.consumedTime = this.consumedTime + value;
	}
	
	
	public void setPunishment(PunishmentType type) {
		this.punishment = type;
	}
	
	public void setDisplay(boolean b) {
		this.bar.setVisible(b);
	}
	
	public boolean isRunning() {
		return this.run;
	}
	
	public PunishmentType getPunishment() {
		return this.punishment;
	}
	
	public boolean isDisplaying() {
		return this.bar.isVisible();
	}
	
	
	public void cancel() {
		super.cancel();
		
		world.getWorldBorder().setWarningDistance(5);
		this.bar.removeAll();
	}
	
	
	
	private void changeBossBar(int secondsLeft) {
		 
		double timeScaled = Math.max(0.0, Math.min(1.0, ((double) secondsLeft) / ((double) maxTime)  ));
		this.bar.setProgress(timeScaled);
		
		int hours = (int) secondsLeft / 3600;
		int minutes = (int) ((secondsLeft - hours * 3600) / 60);
		int seconds = secondsLeft - hours * 3600 - minutes * 60;
		
		bar.setTitle("Time Left: " + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
		

		if(secondsLeft >= maxTime * 0.4) {
			world.getWorldBorder().setWarningDistance(5);
			bar.setColor(BarColor.GREEN);
		}		
		
		if( maxTime * 0.1 <= secondsLeft && secondsLeft < maxTime * 0.4) {
			world.getWorldBorder().setWarningDistance(5);
			bar.setColor(BarColor.YELLOW);
		}
		
		if(secondsLeft < maxTime * 0.1) {
			world.getWorldBorder().setWarningDistance(Integer.MAX_VALUE);
			bar.setColor(BarColor.RED);
		}
	}


	@SuppressWarnings("deprecation")
	private void outOfTime() {
		
		List<Player> players = world.getPlayers();

		
		switch (punishment) {
		case NOTHING:
			break;
			
		case DEATH:
			if(players.isEmpty()) break;
			for(Player p : players) {
				if(p.getGameMode().equals(GameMode.CREATIVE)) continue;
				if(p.getGameMode().equals(GameMode.SPECTATOR)) continue;

				if(!p.isDead() && !(p.getHealth() <= 0)) {
					p.setHealth(0);
					p.setLastDamageCause(new EntityDamageEvent(p, DamageCause.LIGHTNING, 134679) );
				}
			}
			break;
			
		case WITHER:
			if(players.isEmpty()) break;
			for(Player p : players) {
				if(p.getGameMode().equals(GameMode.CREATIVE)) continue;
				if(p.getGameMode().equals(GameMode.SPECTATOR)) continue;
				
				applyEffect(p, PotionEffectType.WITHER);
			}
			break;
			
		case POISON:
			if(players.isEmpty()) break;
			for(Player p : players) {
				if(p.getGameMode().equals(GameMode.CREATIVE)) continue;
				if(p.getGameMode().equals(GameMode.SPECTATOR)) continue;
				
				applyEffect(p, PotionEffectType.POISON);
			}
			break;
			
		case HUNGER:
			if(players.isEmpty()) break;
			for(Player p : players) {
				if(p.getGameMode().equals(GameMode.CREATIVE)) continue;
				if(p.getGameMode().equals(GameMode.SPECTATOR)) continue;
				
				applyEffect(p, PotionEffectType.HUNGER);
			}
			break;
		}
	}
	
	private void applyEffect(Player p, PotionEffectType type) {
		
		if(!p.hasPotionEffect(type)) {
			
			p.addPotionEffect(new PotionEffect(type, 40, 0));
			
		} else {
			
			for(PotionEffect effect : p.getActivePotionEffects().toArray(new PotionEffect[0])) {
				if(!effect.getType().equals(type)) continue;
				if(effect.getAmplifier() > 0) continue;
				if(effect.getDuration() > 20) continue;
				
				p.removePotionEffect(type);
				p.addPotionEffect(new PotionEffect(type, 40, 0));
				break;
			}
			
		}
		
	}
	
}
