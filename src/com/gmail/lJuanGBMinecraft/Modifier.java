package com.gmail.lJuanGBMinecraft;

import java.lang.reflect.Field;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

/**
 * Modifiers that apply when the dungeon is being used.
 * 
 * @author lJuanGB
 */
public enum Modifier {

	DOUBLE_TROUBLE("There is a chance for every mob that spawns to spawn another of the same type.", 0.1),
	HIGH_DOUBLE_TROUBLE("There is a higher chance for every mob that spawns to spawn another of the same type.", 0.25),
	
	LIVING_DEAD("There is a chance zombies recover to full health once they are killed", 0.05),
	
	MORE_PIGS("There is a chance for every zombie that it's converted to a pigman.", 0.05),
	EVEN_MORE_PIGS("There is a higher chance for every zombie that it's converted to a pigman.", 0.1),
	TOO_MANY_PIGS("There is a very high chance for every zombie that it's converted to a pigman.", 0.2),
	NO_PIGS("Every pigman that spawns is replaced with a normal zombie"),
	
	HEAL_AT_ZOMBIE_DEATH("When a zombie dies there is a chance a healing splash potion drops", 0.05),
	HARM_AT_ZOMBIE_DEATH("When a zombie dies there is a chance a damaging splash potion drops", 0.05),
	HEAL_AT_PLAYER_DEATH("When a player dies all the players in the dungeon receive a regeneration effect"),
	HARM_AT_PLAYER_DEATH("When a player dies all the players in the dungeon receive an instant damage effect"),
	KILL_AT_PLAYER_DEATH("When a player dies all the players in the dungeon also die"),

	ZOMBIE_BATTLE_RAGE("When a zombie is hit bellow 30% of its total health it gains speed and strength 1"),
	PLAYER_BATTLE_RAGE("When a player is hit bellow 30% of its total health it gains speed and strength 1"),
	
	KING_PIGMAN("There is a chance a 'King Pigman' spawns, which drops 4 baby pigs at death", 0.025, "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDQyZmYyMzNjMzllNGFiN2QxZTJjY2IyNzMyYWExN2NiNWE2NjhmMjU1ZmZkYTMyYzFlOTcwZmY4NmQ1MmMifX19"),
	
	PINATA("There is a chance a hit zombie dies instanly in a puff of color!", 0.025),
	SUPER_PINATA("All hit zombie die instanly in a puff of color!"),
	DEFINITELY_NOT_ZOMBIES("Instead of zombies, Steves spawn! I think...", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDQyYjE2MmI0ZTJkMjNhYmE0OThlNWM1MTAyNzMxNTgyMTc0MjMyYmQxYWViMDc3OTA3OTEwM2ZkNDdlZGQ4YSJ9fX0="),
	SUMMER("Zombies spawn with the necessary equipment for summer!","eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTVkZTBlZmM1OWJiMjk3NGJkYzMwNWFmMmUxYTJhZWFhZGVkNTQ3YmE5N2ZlMmI1YzRlMzE3MjRiYmUzNjlkIn19fQ=="),
	CHRISTMAS("Zombies spawn with the festivities in mind!","eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjJkNzU3Yjc1NDk1ZDdhNDdiZGRhNWZlNzFjZDk2Y2JmZDExYWVmMTIyNWM4NmJhZGI3NjJiZWM5MDIxNDQifX19"),
	
	;
	
	private Random generator = new Random(System.currentTimeMillis());

	private String description;
	private String head;
	private double chance;
	
	private Modifier(String description, double chance, String head) {
		this.description = description;
		this.head = head;
		this.chance = chance;
	}
	
	private Modifier(String description, String head) {
		this(description, 1, head);
	}
	
	private Modifier(String description, double chance) {
		this(description, chance, null);
	}
	
	private Modifier(String description) {
		this(description, 1);
	}
	

	
	public String getDescription() {
		return this.description;
	}
	
	public boolean hasHead() {
		return (this.head != null);
	}
	
	public String getHeadString() {
		return this.head;
	}
	
	public ItemStack getHead(){
		
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        if (!this.hasHead()) return head;
       
        SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
       
        profile.getProperties().put("textures", new Property("textures", this.getHeadString()));
       
        try
        {
            Field profileField = headMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(headMeta, profile);
           
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
        head.setItemMeta(headMeta);
        return head;
        
    }
	
	
	public double getChance() {
		return this.chance;
	}
	
	public boolean shouldHappen() {
		return (chance > generator.nextDouble());
	}
}
